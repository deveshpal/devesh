<?php

function partisosialis_preprocess_page(&$vars) {
  global $theme_key;
  $theme_name = $theme_key;
  $logo_path = check_url($vars['logo']);
  $logo_alt = check_plain(variable_get('site_name', t('Site logo')));
  $logo_vars = array('path' => $logo_path, 'alt' => $logo_alt, 'attributes' => array('class' => 'site-logo'));
  $vars['logo_img'] = theme('image', $logo_vars);
  $vars['logo'] = $vars['logo_img'] ? l($vars['logo_img'], '<front>', array('attributes' => array('title' => t('Home page')), 'html' => TRUE)) : '';
}