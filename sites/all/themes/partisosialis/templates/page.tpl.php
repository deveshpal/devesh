<div id="page-wrapper">
  <div id="page" class="<?php print $classes; ?>">

    <!-- Page top -->
    <?php if ($page['page_top']): ?>
      <div id="menu-bar-wrapper">
        <div class="container clearfix">
          <?php print render($page['page_top']); ?>  
        </div>
      </div>
    <?php endif; ?>

    <!-- Header Region -->
    <div id="header-wrapper"  >
      <div class="container clearfix">
        <header class="clearfix<?php print $logo ? ' with-logo' : ''; ?>" role="banner">
          <?php if ($logo || $site_name || $site_slogan): ?>
            <div id="branding" class="branding-elements clearfix">
              <?php if ($logo): ?>
                <div id="logo">
                  <?php print $logo; ?>
                </div>
              <?php endif; ?>
            </div>
          <?php endif; ?>
          <?php print render($page['header']); ?>
        </header>
      </div>
    </div> 

    <!-- Menu Region -->
    <div id="menu-bar">
      <div class="menu-container clearfix">
        <?php print render($page['menu'])?>
      </div>
    </div>

    <div id="highlighted">
      <div class="highlighted-container clearfix">
        <?php print render($page['highlighted']); ?>
         <?php if(!drupal_is_front_page()) print $title;?>

      </div>
    </div>

    <?php if ($messages || $page['help']): ?>
      <div id="messages-help-wrapper">
        <div class="container clearfix">
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
        </div>
      </div>
    <?php endif; ?>

    <div id="main-wrapper" class="main-div-wrapper">
      <div class="content-wrapper wrapper">
        <div class="main-content">
          <!-- breadcrumb -->
          <?php if ($breadcrumb): print $breadcrumb; endif; ?>
          <!-- page-title -->
                   <?php if ($action_links = render($action_links)): ?>
            <ul class="action-links">
              <?php print $action_links; ?>
            </ul>
          <?php endif; ?>
          <div id="content">
             <?php print render($page['content']); ?>
          </div>
          <?php print $feed_icons; ?>  

        </div>
      </div>
      <?php if($page['sidebar_first']): ?>
      <div class="sidebar-first-wrapper wrapper">
        <div class="sidebar-first-content">
          <?php print render($page['sidebar_first']); ?>
        </div>
      </div>
       <?php endif;?>
      <?php if($page['sidebar_second']):?>
      <div class="sidebar-second-wrapper wrapper">
        <div class="sidebar-second-content">
          <?php print render($page['sidebar_second']); ?>
        </div>
      </div>
      <?php endif;?>
    </div>

    <!-- Footer -->
    <?php if ($page['footer']): ?>
      <div id="footer-wrapper">
        <div class="container clearfix">
          <footer class="clearfix" role="contentinfo">
            <?php print render($page['footer']); ?>
          </footer>
        </div>
      </div>
    <?php endif; ?>

  </div>
</div>
